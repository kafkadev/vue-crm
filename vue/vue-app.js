var HeaderFirst = httpVueLoader('./vue/part/header.vue')
var HeaderSort = httpVueLoader('./vue/part/header-sort.vue')
var Footer = httpVueLoader('./vue/part/footer.vue')
var Sidebar = httpVueLoader('./vue/part/sidebar.vue');
var App = httpVueLoader('./vue/container/home.vue');


var Blank = httpVueLoader('./vue/page/blank.vue');
var Dashboard = httpVueLoader('./vue/page/dashboard.vue');
var PageMenu = httpVueLoader('./vue/menu/menu.vue');
var PageTable = httpVueLoader('./vue/page/table.vue');
var PageOrder = httpVueLoader('./vue/order/list-order.vue');


//Categories
var PageCategories = httpVueLoader('./vue/categories/categories.vue');


//Ads
var PageAds = httpVueLoader('./vue/ads/ads.vue');
var PageEditAd = httpVueLoader('./vue/ads/edit-ad.vue');

//Posts
var PagePosts = httpVueLoader('./vue/posts/posts.vue');
var PageEditPost = httpVueLoader('./vue/posts/edit-post.vue');


//Pages

var PageInformation = httpVueLoader('./vue/page/information.vue');
var PageBook = httpVueLoader('./vue/page/book.vue');
var PageContent = httpVueLoader('./vue/page/content.vue');
var PageCustomer = httpVueLoader('./vue/page/customer.vue');
var PageLanguage = httpVueLoader('./vue/page/language.vue');
var PageNewsletter = httpVueLoader('./vue/page/newsletter.vue');
var PageOffer = httpVueLoader('./vue/page/offer.vue');
var PageProfile = httpVueLoader('./vue/page/profile.vue');
var PageReport = httpVueLoader('./vue/page/report.vue');
var PageReview = httpVueLoader('./vue/page/review.vue');
var PageSocial = httpVueLoader('./vue/page/social.vue');
var PageTools = httpVueLoader('./vue/page/tools.vue');
var PageVoucher = httpVueLoader('./vue/page/voucher.vue');




var routes = [
{
  path: '/dashboard',
  name: 'DASHBOARD',
  component: Dashboard
},
{
  path: '/dashboard/ads',
  name: 'ADS',
  component: PageAds
},
{
    path: '/dashboard/ads/:ad_id',
    name: 'AD_EDIT',
    component: PageEditAd
},
{
  path: '/dashboard/posts',
  name: 'POSTS',
  component: PagePosts
},
{
    path: '/dashboard/posts/:post_id',
    name: 'POST_EDIT',
    component: PageEditPost
},
{
  path: '/dashboard/categories',
  name: 'CATEGORIES',
  component: PageCategories
}
];

var router = new VueRouter({
  //mode: 'history',
  routes: routes
});


var app = new Vue({
  el: "#app",
  router: router,
  template: "<App/>",
    data() {
        return {};
    },
    mixins: [],
    components: {
        App
    },
    beforeCreate() {},
    created() {},
    mounted() {
/*
      fetch('http://rest.me/api/table/rst_restaurant_meta')
      .then(response => response.json())
      .then(res => {
        localforage.setItem('site_settings_' + localStorage.getItem('restaurant_id'), res, console.log);
      })
*/

    },
    methods: {
        buttonClick: function() {
            // this.$store.commit('increment')
        }
    }
});
