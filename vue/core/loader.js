const ScriptStore = [
    { name: 'select2', src: 'assets/libs/select2/js/select2.min.js' },
    { name: 'flatpickr', src: 'assets/libs/flatpickr/flatpickr.min.js' },
    { name: 'touchspin', src: 'assets/libs/bootstrap-touchspin/jquery.bootstrap-touchspin.min.js' },
    { name: 'jscrollpane', src: 'assets/libs/jscrollpane/jquery.jscrollpane.min.js' },
    { name: 'mousewheel', src: 'assets/libs/jscrollpane/jquery.mousewheel.js' },
    { name: 'd3', src: 'assets/libs/d3/d3.min.js' },
    { name: 'c3', src: 'assets/libs/c3js/c3.min.js' },
    { name: 'noty', src: 'assets/libs/noty/noty.min.js' },
    { name: 'maplace', src: 'assets/libs/maplace/maplace.min.js' },
    { name: 'fullcalendar', src: 'assets/libs/fullcalendar/fullcalendar.min.js' },
    { name: 'momentjs', src: 'assets/js/lib/moment/moment-with-locales.min.js' },
    { name: 'plyr', src: 'assets/libs/plyr/plyr.js' },
    { name: 'uppy', src: 'assets/libs/uppy/uppy.min.js' },
    { name: 'dropzone', src: 'assets/libs/dropzone/dropzone.min.js' },
    { name: 'colorpicker', src: 'assets/libs/jquery-minicolors/jquery.minicolors.min.js' },
    { name: 'inputmask', src: 'assets/libs/jquery-mask/jquery.mask.min.js' },
];
let scripts = {}
ScriptStore.forEach((script) => {
    scripts[script.name] = {
        loaded: false,
        src: script.src
    };
});


function scriptLoad(name, cb) {


    if (!scripts[name].loaded) {
        var req = new XMLHttpRequest();
        req.open('GET', scripts[name].src, false);
        req.onreadystatechange = function() {
            if (req.readyState == 4) {
                var s = document.createElement("script");
                s.appendChild(document.createTextNode(req.responseText));
                scripts[name].loaded = true;
                document.head.appendChild(s);
            }
        };
        req.send(null);
    }
}


function loadScript(url, callback) {
    // adding the script tag to the head as suggested before
    var head = document.getElementsByTagName('head')[0];
    var script = document.createElement('script');
    script.type = 'text/javascript';
    script.src = url;

    // then bind the event to the callback function
    // there are several events for cross browser compatibility
    script.onreadystatechange = callback;
    script.onload = callback;

    // fire the loading
    head.appendChild(script);
}

let yuklenenler = {}

function loadScript2(url, callback) {
    let basladi = Date.now()
    if (!yuklenenler[url]) {
        var script = document.createElement("script")
        script.type = "text/javascript";

        if (script.readyState) { //IE
            script.onreadystatechange = function() {
                if (script.readyState == "loaded" || script.readyState == "complete") {
                    script.onreadystatechange = null;
                    callback('yuklendi');
                    yuklenenler[url] = true
                    console.log("yuklendi", Date.now() - basladi)

                }
            };
        } else { //Others
            script.onload = function() {
                callback('yuklendi');
                yuklenenler[url] = true
                console.log("yuklendi", Date.now() - basladi)
            };
        }

        script.src = url;
        document.getElementsByTagName("head")[0].appendChild(script);
    } else {
        callback('zaten var')
        console.log("zaten var", Date.now() - basladi)
    }

}


Vue.directive("sortable", {
    bind: (el) => {
        setTimeout(() => {
            $(el).sortable();
        }, 600)
    }
});

Vue.directive("scrollable", {
    bind: (el) => {
        setTimeout(() => {
            var jScrollOptions = {
                autoReinitialise: true,
                autoReinitialiseDelay: 100,
                contentWidth: '0px'
            };
            $(el).jScrollPane(jScrollOptions);
            //jscroll.destroy();
        }, 600)
    }
});

Vue.directive("selectable", {
    bind: (el) => {
        setTimeout(() => {
            $(el).selectpicker({
                style: '',
                width: '100%',
                size: 8
            });
        }, 600)
    }
});

Vue.directive("tooltip", {
    bind: (el) => {
        setTimeout(() => {
            // Tooltip
            $(el).tooltip({
                html: true
            });


        }, 100)
    }
});

Vue.directive("popover", {
    bind: (el) => {
        setTimeout(() => {
            // Popovers
            $(el).popover({
                trigger: 'focus'
            });
        }, 100)
    }
});



Vue.directive("boxTypical", {
    bind: (el) => {
        setTimeout(() => {

            var parent = $(el),
                btnCollapse = parent.find('.action-btn-collapse'),
                btnExpand = parent.find('.action-btn-expand'),
                classExpand = 'box-typical-full-screen';

            btnCollapse.click(() => {
                if (parent.hasClass('box-typical-collapsed')) {
                    parent.removeClass('box-typical-collapsed');
                } else {
                    parent.addClass('box-typical-collapsed');
                }
            });




            btnExpand.click(() => {
                if (parent.hasClass(classExpand)) {
                    parent.removeClass(classExpand);
                    $('html').css('overflow', 'auto');
                } else {
                    parent.addClass(classExpand);
                    $('html').css('overflow', 'hidden');
                }
            });
        }, 600)
    }
});

Vue.filter("json", value => {
    if (!value) return value;
    return JSON.stringify(value, null, 2);
});

axios.defaults.baseURL = '//core.me/api';
axios.defaults.headers.common['Authorization'] = "Bearer 123456";




Vue.mixin({
    data() {
        return {
            defaultData2: "aslan",
            informations: {
                root: 'deneme'
            }
        };
    },
    methods: {
        updateRoot: function(key_name, key_value) {
            this.informations[key_name] = key_value
        },
        getLocalData: function(meta_key, cb) {
            localforage.getItem('site_settings_' + localStorage.getItem('restaurant_id'))
                .then(data => cb(data[meta_key]))
        },
        axios_test: function(url, pdata, cb) {
            axios.post(url, pdata)
                .then(function(response) {
                    cb(response)
                })
                .catch(function(error) {
                    console.log(error);
                });
        },
        post: function(url, pdata, cb) {

            fetch(url, {
                headers: {
                    "content-type": "application/json"
                },
                method: "post",
                body: JSON.stringify(pdata)
            }).then((response) => {
                return response.json();
            }).then((data) => {
                cb(data)
            });
        }
    },
    computed: {
        infor_data: function() {
            return this.informations
        }
    },
    mounted() {

    },
    beforeCreate() {},
    created() {},
    beforeUpdate() {},
    updated() {},
    filters: {},
    components: {},
    store: new Vuex.Store({
        state: {
            count: 100,
            genel: {
                bilgi: 'yok'
            },
            ekle2: {},
            datalar: {}
        },
        mutations: {
            increment({ state }) {
                state.count++
            },

            dataEkle(state, data) {
                state.datalar = data
            }

        },
        actions: {
            ekle({ commit, state }, bilgi) {
                state.genel.yeni = bilgi
            },
            ekle2({ commit, state }, bilgi) {
                state.ekle2 = bilgi
            }
        }
    })
});

localStorage.setItem("restaurant_id", 3);

localforage.config({
    driver: localforage.INDEXEDDB, // Force WebSQL; same as using setDriver()
    name: 'restaurant_manage',
    version: 1.0,
    storeName: 'restaurant_db', // Should be alphanumeric, with underscores.
    description: 'açıklama'
});

var store = localforage.createInstance({
    driver: localforage.INDEXEDDB, // Force WebSQL; same as using setDriver()
    name: 'restaurant_1',
    version: 1.0,
    storeName: 'restaurant_1_db', // Should be alphanumeric, with underscores.
    description: 'açıklama'
});